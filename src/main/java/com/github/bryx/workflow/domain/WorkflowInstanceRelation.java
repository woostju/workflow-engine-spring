package com.github.bryx.workflow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "WORKFLOW_INST_REL", autoResultMap = true)
public class WorkflowInstanceRelation {
    public enum Type{
        SYNC,
        ASYNC
    }

    @ApiModelProperty(value = "id")
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    String id;

    @ApiModelProperty(value = "流程实例序号")
    @TableField(value = "WORKFLOW_INST_ID")
    String workflowInstanceId;

    @ApiModelProperty(value = "流程Task Def id")
    @TableField(value = "PROCESS_TASK_DEF_ID")
    String processTaskDefId;

    @ApiModelProperty(value = "流程实例任务实例id")
    @TableField(value = "WORKFLOW_INST_TASK_ID")
    String workflowTaskInstanceId;

    @ApiModelProperty(value = "子流程实例id")
    @TableField(value = "SUB_WORKFLOW_INST_ID")
    String subWorkflowInstanceId;

    @ApiModelProperty(value = "关系类型")
    @TableField(value = "TYPE")
    Type type;
}
