package com.github.bryx.workflow.domain;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/5/25
 **/
@Data
public class WorkflowDefField {
    @Data
    public static class Option{
        String name;
        Object value;
    }
    public enum WorkflowFieldType{
        TEXT_INPUT,
        TEXT_AREA,
        RICH_TEXT,
        RADIO,
        CHECKBOX,
        DROPDOWN_LIST_SINGLE,
        DROPDOWN_LIST_MULTIPLE,
        DATETIME_PICKER,
        PIC,
        ATTACHMENT
    }
    public enum WorkflowFieldValueType{
        INT_,
        LONG_,
        STRING_,
        PASSWORD_,
        FLOAT_,
        BOOLEAN_,
        DOUBLE_,
        DATETIME_,
        DATE_
    }
    @ApiModelProperty(value="id", notes = "不同节点不可变", required = true)
    String id;
    @ApiModelProperty(value="字段控件类型", notes = "不同节点不可变")
    WorkflowFieldType fieldType;
    @ApiModelProperty(value="字段值类型", notes = "不同节点不可变")
    WorkflowFieldValueType fieldValueType;
    @ApiModelProperty(value="名称", notes = "不同节点可变")
    String name;
    @ApiModelProperty(value="显示名称", notes = "不同节点可变")
    String displayName;
    @ApiModelProperty(value="placeholder", notes = "不同节点可变")
    String placeholder;
    @ApiModelProperty(value="可选项", notes = "不同节点可变")
    List<Option> options;
    @ApiModelProperty(value="值", notes = "覆盖提交的值")
    Object value;
    @ApiModelProperty(value="默认值", notes = "不同节点可变")
    Object defaultValue;
    @ApiModelProperty(value="必填", notes = "不同节点可变, js script")
    String required;
    @ApiModelProperty(value="校验配置", notes = "不同节点可变")
    String validation;
    @ApiModelProperty(value="是否可见", notes = "不同节点可变, js script")
    String visible;
    @ApiModelProperty(value="是否可编辑", notes = "不同节点可变, js script")
    String editable;
    @ApiModelProperty(value="是否添加到历史中", notes = "不同节点可变")
    Boolean activity;
    @ApiModelProperty(value="字段配置扩展", notes = "不同节点不可变")
    JSONObject extension;

    public String getDisplayName() {
        return displayName == null? name:displayName;
    }
}
