package com.github.bryx.workflow.domain;

/**
 * @Author jameswu
 * @Date 2021/5/25
 **/
public enum WorkflowUserOperationType {
    START("发起"),
    REJECT_BACK("驳回"), // 驳回
    REJECT_BACK_ASSIGN("驳回并指派"), // 驳回并指定受理人
    CLAIM("认领"), // 认领
    TRANSFER("转移"), // 转移
    MODIFY("编辑"), // 修改
    SUBMIT("提交"), // 提交
    SUBMIT_ASSIGN("提交并指派"), // 提交并指定受理人
    CLOSE("关闭"); // 关闭

    private String name;

    WorkflowUserOperationType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
