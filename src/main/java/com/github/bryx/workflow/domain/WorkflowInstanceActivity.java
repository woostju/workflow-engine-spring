package com.github.bryx.workflow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.github.bryx.workflow.config.JsonTypeHandler;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName(value = "WORKFLOW_INST_ACTIVITY", autoResultMap = true)
public class WorkflowInstanceActivity {

    @Data
    public static class ActivityContent{
        List<FormField> formFields = Lists.newArrayList();
        String description;
    }

    @Data
    public static class FormField{
        String name;
        Object value;
    }

    @ApiModelProperty(value = "id")
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    String id;

    @ApiModelProperty(value = "实例id")
    @TableField(value = "WORKFLOW_INST_ID")
    String workflowInstanceId;

    @ApiModelProperty(value = "任务Name")
    @TableField(value = "WORKFLOW_TASK_NAME")
    String workflowTaskName;

    @ApiModelProperty(value = "流程task id")
    @TableField(value = "WORKFLOW_TASK_ID")
    String workflowTaskId;

    @ApiModelProperty(value = "操作人id")
    @TableField(value = "OPERATOR_ID")
    String operatorId;

    @ApiModelProperty(value = "操作名称")
    @TableField(value = "OPERATE_NAME")
    String operateName;

    @ApiModelProperty(value = "操作时间")
    @TableField(value = "OPERATE_TIME")
    Date operateTime;

    @ApiModelProperty(value = "历史附属内容")
    @TableField(value = "CONTENT", typeHandler = JsonTypeHandler.class)
    ActivityContent content = new ActivityContent();

}
