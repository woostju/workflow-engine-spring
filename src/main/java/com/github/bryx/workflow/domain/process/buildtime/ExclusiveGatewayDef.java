package com.github.bryx.workflow.domain.process.buildtime;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ExclusiveGatewayDef extends ProcessDefElement {
    List<SequenceDef> incomingFlows;
}
