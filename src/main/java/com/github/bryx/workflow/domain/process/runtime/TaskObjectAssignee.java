package com.github.bryx.workflow.domain.process.runtime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author jameswu
 * @Date 2021/5/18
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "ACT_TASK_ASSIGNEE")
public class TaskObjectAssignee {
    @TableField(value = "TASK_ID_")
    private String taskId;
    @TableField(value = "ASSIGNEE_USER_ID_")
    private String assigneeUserId;
    @TableField(value = "ASSIGNEE_GROUP_ID_")
    private String assigneeGroupId;
}
