package com.github.bryx.workflow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "WORKFLOW_INST_TIMER_INST", autoResultMap = true)
public class WorkflowTimerInstance {

    @ApiModelProperty(value = "id")
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    String id;

    @ApiModelProperty(value = "timer job id")
    @TableField(value = "WORKFLOW_TIMER_JOB_ID")
    String workflowTimerJobId;

    @ApiModelProperty(value = "instance id")
    @TableField(value = "WORKFLOW_INST_ID")
    String workflowInstanceId;

    @ApiModelProperty(value = "流程task id")
    @TableField(value = "WORKFLOW_TASK_ID")
    String workflowTaskId;

    @ApiModelProperty(value = "触发时间")
    @TableField(value = "TRIGGER_TIME")
    private Date triggerTime;

}
