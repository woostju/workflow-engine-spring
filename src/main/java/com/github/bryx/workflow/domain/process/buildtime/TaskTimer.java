package com.github.bryx.workflow.domain.process.buildtime;

import com.github.bryx.workflow.domain.process.runtime.TaskObject;
import lombok.Data;

import java.util.Date;

@Data
public class TaskTimer {
	private String jobId;
	private String definitionId;
	private Date triggerTime;
	private TaskObject task;
}
