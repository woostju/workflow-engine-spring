package com.github.bryx.workflow.handler;

import com.github.bryx.workflow.command.BaseCommand;
import com.github.bryx.workflow.domain.*;
import com.github.bryx.workflow.domain.process.runtime.TaskObject;
import com.github.bryx.workflow.domain.process.runtime.TaskObjectAssignee;
import com.github.bryx.workflow.event.WorkflowEvent;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/11
 **/
public interface WorkflowInstanceAware {

    /**
     * 处理事件
     * Sample Usage: send notification after user submit
     * @param event
     */
    public void handleEvent(String processDefKey, WorkflowEvent event);

    /**
     *
     * @param processDefKey
     * @param userId
     * @param taskInstance
     * @return 用户在流程节点上的操作
     *
     */
    default public List<WorkflowDefProcessConfig.UserOperation> userActionsOnTask(String processDefKey, String userId, WorkflowTaskInstance taskInstance, WorkflowDefProcessConfig.UserTaskConfig userTaskConfig){
        return userTaskConfig.getUserActions();
    }


    /**
     *
     * 在命令被执行前被调用
     * Sample Usage: change form data before command execute
     * @param processDefKey
     * @param command
     *
     */
    default public void preCommandExecute(String processDefKey, BaseCommand command){
        return;
    }

    /**
     *
     * 自定义任务的受理人设置，当用户不指定受理人
     * 当指定受理人时，不被调用
     * @param workflowInstance
     * @param userTaskConfig
     * @return
     */
    public List<TaskObjectAssignee> userTaskAssign(String processDefKey, WorkflowInstance workflowInstance, WorkflowTaskInstance executionTaskInstance, WorkflowDefProcessConfig.UserTaskConfig userTaskConfig);

    /**
     * timerconfig配置了extension手动创建timer
     * @param workflowInstance
     * @param timerConfig
     */
    default public void createTimerJobWhenExtensionConfigured(String processDefKey, WorkflowInstance workflowInstance, WorkflowTaskInstance taskInstance, WorkflowDefProcessConfig.TimerConfig timerConfig){

    }
}
