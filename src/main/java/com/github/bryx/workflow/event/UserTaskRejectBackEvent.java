package com.github.bryx.workflow.event;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/11
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTaskRejectBackEvent implements WorkflowEvent{

    private WorkflowTaskInstance workflowTaskInstance;
    private List<WorkflowTaskInstance> newWorkflowTaskInstances;
    private JSONObject formData;

    @Override
    public Type getType() {
        return Type.USER_TASK_REJECT_BACK;
    }
}
