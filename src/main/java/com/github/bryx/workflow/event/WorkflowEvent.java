package com.github.bryx.workflow.event;

import com.alibaba.fastjson.util.TypeUtils;

/**
 * @Author jameswu
 * @Date 2021/5/25
 **/
public interface WorkflowEvent {
    public enum Type{
        USER_TASK_CLAIM,
        USER_TASK_SUBMIT,
        USER_TASK_ENTER,
        USER_TASK_MODIFY,
        USER_TASK_REJECT_BACK,
        USER_TASK_TRANSFER,
        WORKFLOW_CLOSE,
        WORKFLOW_START,
        TIMER_TRIGGER
    }

    Type getType();
    
    default public <T> T as(Class<T> elementClass){
        return TypeUtils.castToJavaBean(this, elementClass);
    }
}
