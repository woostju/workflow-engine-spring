package com.github.bryx.workflow.event;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author jameswu
 * @Date 2021/7/8
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTaskEnterEvent implements WorkflowEvent{

    private WorkflowTaskInstance workflowTaskInstance;
    private WorkflowTaskInstance previousTaskInstance;
    private JSONObject formData;

    @Override
    public Type getType() {
        return Type.USER_TASK_ENTER;
    }
}
