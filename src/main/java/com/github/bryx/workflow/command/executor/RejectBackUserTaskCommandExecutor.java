package com.github.bryx.workflow.command.executor;

import com.github.bryx.workflow.command.BaseCommand;
import com.github.bryx.workflow.command.RejectBackUserTaskCommand;
import com.github.bryx.workflow.domain.*;
import com.github.bryx.workflow.domain.process.runtime.TaskObject;
import com.github.bryx.workflow.domain.process.runtime.TaskObjectAssignee;
import com.github.bryx.workflow.dto.runtime.CreateWorkflowInstanceActivityDto;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowInstanceDto;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowTaskInstanceDto;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowTimerJobDto;
import com.github.bryx.workflow.event.UserTaskEnterEvent;
import com.github.bryx.workflow.event.UserTaskRejectBackEvent;
import com.github.bryx.workflow.handler.WorkflowInstanceAware;
import com.github.bryx.workflow.service.WorkflowBuildTimeService;
import com.github.bryx.workflow.service.WorkflowRuntimeService;
import com.github.bryx.workflow.service.process.ProcessService;
import com.github.bryx.workflow.util.WorkflowEngineUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RejectBackUserTaskCommandExecutor extends CommandExecutor<List<String>> {

    @Autowired
    WorkflowBuildTimeService workflowBuildTimeService;

    @Autowired
    WorkflowRuntimeService workflowRuntimeService;

    @Autowired
    ProcessService processService;

    @Override
    public List<String> run() {
        RejectBackUserTaskCommand command = this.getCommand().as(RejectBackUserTaskCommand.class);
        WorkflowInstance workflowInstance = workflowRuntimeService.query().getWorkflowInstanceById(command.getWorkflowInstanceId());
        WorkflowTaskInstance workflowTaskInstance = workflowRuntimeService.query().getWorkflowTaskInstanceById(command.getWorkflowTaskInstanceId());
        WorkflowDefRev workflowDefRev = workflowBuildTimeService.query().getWorkflowDefRevById(workflowInstance.getDefRevId());
        WorkflowDef workflowDef = workflowBuildTimeService.query().getWorkflowDefById(workflowDefRev.getDefId());
        workflowDef.setRev(workflowDefRev);

        // 更新任务的状态
        UpdateWorkflowTaskInstanceDto updateTaskDto = UpdateWorkflowTaskInstanceDto.builder()
                .executorId(command.getExecutorId())
                .status(WorkflowTaskInstance.WorkflowTaskInstanceStatus.COMPLETED)
                .id(workflowTaskInstance.getId())
                .build();
        workflowRuntimeService.manager().updateWorkflowTaskInstance(updateTaskDto);

        // 更新timer job状态
        workflowRuntimeService.manager().updateWorkflowTimerJob(UpdateWorkflowTimerJobDto.builder()
                .workflowTaskIds(Lists.newArrayList(workflowTaskInstance.getId()))
                .status(WorkflowTimerJob.Status.CLOSED)
                .build());

        // 更新流程实例的表单数据
        UpdateWorkflowInstanceDto updateInstanceDto = UpdateWorkflowInstanceDto.builder()
                .id(workflowInstance.getId())
                .executorId(command.getExecutorId())
                .formData(command.getFormData())
                .build();
        workflowRuntimeService.manager().updateWorkflowInstance(updateInstanceDto);

        // 分配用户，驳回并指定受理人
        final Map<String, WorkflowDefProcessConfig.UserTaskConfig> userTasksConfig = workflowDefRev.getProcessConfig().getUserTasks();
        List<String> newTaskIds = processService.rejectBack(workflowInstance.getProcessId(), workflowTaskInstance.getProcessTaskId(), command.getExecutorId(),command.getFormData(), (tasks, processObject) -> {
            Map<String, List<TaskObjectAssignee>> taskObjectAssigneesMap = Maps.newHashMap();
            tasks.forEach(taskObject -> {
                userTasksConfig.get(taskObject.getDefinitionId()).setTaskDefId(taskObject.getDefinitionId());
                List<TaskObjectAssignee> taskObjectAssignees = Optional.ofNullable(WorkflowEngineUtil.createTaskObjectAssignees(command.getAssigneeUserIds(), command.getAssigneeGroupIds()))
                        .orElse(this.getWorkflowInstanceAware().userTaskAssign(workflowDef.getProcessDefKey(), workflowInstance, workflowTaskInstance, userTasksConfig.get(taskObject.getDefinitionId())));
                taskObjectAssigneesMap.put(taskObject.getId(), Optional.ofNullable(taskObjectAssignees).orElse(Lists.newArrayList()));
            });
            return taskObjectAssigneesMap;
        });

        // 数据库记录任务，并发送用户提交事件
        List<TaskObject> tasks = processService.getTasks(newTaskIds);
        List<WorkflowTaskInstance> newWorkflowTaskInstances = this.getCommandExecutorHelper()
                .createWorkflowTaskInstances(workflowInstance.getId(), workflowDefRev, tasks);
        // 创建timer
        this.getCommandExecutorHelper().createWorkflowTimerJobsIfNecessary(workflowDef.getProcessDefKey(), workflowInstance, newWorkflowTaskInstances, workflowDefRev.getProcessConfig(), this.getWorkflowInstanceAware());

        // 发送 UserTaskRejectBackEvent
        this.getWorkflowInstanceAware().handleEvent(workflowDef.getProcessDefKey(), UserTaskRejectBackEvent.builder()
                .workflowTaskInstance(workflowTaskInstance)
                .newWorkflowTaskInstances(newWorkflowTaskInstances)
                .formData(command.getFormData())
                .build());

        // 发送UserTaskEnterEvent
        newWorkflowTaskInstances.forEach(taskInstance->{
            this.getWorkflowInstanceAware().handleEvent(workflowDef.getProcessDefKey(), UserTaskEnterEvent.builder()
                    .previousTaskInstance(workflowTaskInstance)
                    .workflowTaskInstance(taskInstance)
                    .formData(command.getFormData())
                    .build());
        });

        // 创建历史
        CreateWorkflowInstanceActivityDto createWorkflowInstanceActivityDto = CreateWorkflowInstanceActivityDto.builder()
                .operatorId(command.getExecutorId())
                .workflowInstanceId(workflowTaskInstance.getWorkflowInstanceId())
                .operateName(WorkflowUserOperationType.REJECT_BACK.getName())
                .workflowTaskName(workflowTaskInstance.getName())
                .workflowTaskId(workflowTaskInstance.getId())
                .build();
        createWorkflowInstanceActivityDto.extractActivityFormData(workflowTaskInstance.getProcessTaskDefId(), workflowDefRev.getProcessConfig(), command.getFormData());
        this.getCommandExecutorHelper().createActivity(createWorkflowInstanceActivityDto);

        return newWorkflowTaskInstances.stream().map(WorkflowTaskInstance::getId).collect(Collectors.toList());
    }
}
