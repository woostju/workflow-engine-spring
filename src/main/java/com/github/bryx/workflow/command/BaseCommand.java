package com.github.bryx.workflow.command;

import com.alibaba.fastjson.util.TypeUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 *
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseCommand {

    /**
     * processdefkey
     */
    private String processDefKey;

    /**
     * 任务实例id
     */
    private String workflowTaskInstanceId;
    /**
     * 流程实例id
     */
    private String workflowInstanceId;

    public enum CommandType{
        START_COMMAND,
        CLAIM_COMMAND,
        CLOSE_COMMAND,
        MODIFY_COMMAND,
        REJECT_BACK_COMMAND,
        SUBMIT_COMMAND,
        TRANSFER_COMMAND,
        TIMER_TRIGGER_COMMAND,
        FETCH_TASK_CONFIG_COMMAND
    }

    abstract public CommandType getType();

    public <T> T as(Class<T> elementClass){
        return TypeUtils.castToJavaBean(this, elementClass);
    }
}
