package com.github.bryx.workflow.command;

import com.github.bryx.workflow.command.executor.CloseWorkflowInstanceCommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = CloseWorkflowInstanceCommandExecutor.class)
public class CloseWorkflowInstanceCommand extends BaseCommand {
    /**
     * 执行人id
     */
    private String executorId;

    @Override
    public CommandType getType() {
        return CommandType.CLOSE_COMMAND;
    }
}
