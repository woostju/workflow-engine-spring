package com.github.bryx.workflow.command;

import com.github.bryx.workflow.command.executor.CloseWorkflowInstanceCommandExecutor;
import com.github.bryx.workflow.command.executor.FetchTaskConfigCommandExecutor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = FetchTaskConfigCommandExecutor.class)
public class FetchTaskConfigCommand extends BaseCommand {
    /**
     * 执行人id
     */
    private String userId;

    @Override
    public CommandType getType() {
        return CommandType.FETCH_TASK_CONFIG_COMMAND;
    }
}
