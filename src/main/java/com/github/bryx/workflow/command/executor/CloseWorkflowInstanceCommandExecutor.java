package com.github.bryx.workflow.command.executor;

import com.github.bryx.workflow.command.CloseWorkflowInstanceCommand;
import com.github.bryx.workflow.domain.WorkflowDef;
import com.github.bryx.workflow.domain.WorkflowInstance;
import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import com.github.bryx.workflow.domain.WorkflowTimerJob;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowInstanceDto;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowTaskInstanceDto;
import com.github.bryx.workflow.dto.runtime.UpdateWorkflowTimerJobDto;
import com.github.bryx.workflow.event.WorkflowCloseEvent;
import com.github.bryx.workflow.handler.WorkflowInstanceAware;
import com.github.bryx.workflow.service.WorkflowBuildTimeService;
import com.github.bryx.workflow.service.WorkflowRuntimeService;
import com.github.bryx.workflow.service.process.ProcessService;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CloseWorkflowInstanceCommandExecutor extends CommandExecutor<Void> {

    @Autowired
    WorkflowBuildTimeService workflowBuildTimeService;

    @Autowired
    WorkflowRuntimeService workflowRuntimeService;

    @Autowired
    ProcessService processService;

    @SneakyThrows
    @Override
    @Transactional
    public Void run() {
        CloseWorkflowInstanceCommand command = this.getCommand().as(CloseWorkflowInstanceCommand.class);

        WorkflowInstance workflowInstance = workflowRuntimeService.query().getWorkflowInstanceById(command.getWorkflowInstanceId());
        // 更新task instance状态
        List<WorkflowTaskInstance> workflowTaskInstances = workflowRuntimeService.query().getWorkflowTaskInstances(command.getWorkflowInstanceId());
        workflowTaskInstances.forEach(taskInst->{
            UpdateWorkflowTaskInstanceDto updateTaskDto = UpdateWorkflowTaskInstanceDto.builder()
                    .id(taskInst.getId())
                    .status(WorkflowTaskInstance.WorkflowTaskInstanceStatus.INTERRUPTED)
                    .build();
            workflowRuntimeService.manager().updateWorkflowTaskInstance(updateTaskDto);
            taskInst.setStatus(WorkflowTaskInstance.WorkflowTaskInstanceStatus.INTERRUPTED);
        });
        // 更新timer job状态
        workflowRuntimeService.manager().updateWorkflowTimerJob(UpdateWorkflowTimerJobDto.builder()
                .workflowInstanceIds(Lists.newArrayList(workflowInstance.getId()))
                .status(WorkflowTimerJob.Status.CLOSED)
                .build());

        // 关闭process
        processService.closeProcess(workflowInstance.getProcessId(), command.getExecutorId());

        // 更新workflow instance状态
        workflowRuntimeService.manager().updateWorkflowInstance(UpdateWorkflowInstanceDto.builder()
                .id(workflowInstance.getId())
                .executorId(command.getExecutorId())
                .status(WorkflowInstance.WorkflowInstanceStatus.CLOSED)
                .build());

        // 分发消息
        this.getWorkflowInstanceAware().handleEvent(this.getProcessDefKey(), WorkflowCloseEvent.builder()
                .workflowInstance(workflowRuntimeService.query().getWorkflowInstanceById(command.getWorkflowInstanceId()))
                .executorId(command.getExecutorId())
                .interruptedTaskInstances(workflowTaskInstances)
                .build());
        return null;
    }
}
