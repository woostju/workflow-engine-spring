package com.github.bryx.workflow.command.executor;

import com.github.bryx.workflow.command.ClaimUserTaskCommand;
import com.github.bryx.workflow.domain.WorkflowDef;
import com.github.bryx.workflow.domain.WorkflowInstance;
import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import com.github.bryx.workflow.event.UserTaskClaimEvent;
import com.github.bryx.workflow.handler.WorkflowInstanceAware;
import com.github.bryx.workflow.service.WorkflowBuildTimeService;
import com.github.bryx.workflow.service.WorkflowRuntimeService;
import com.github.bryx.workflow.service.process.ProcessService;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ClaimUserTaskCommandExecutor extends CommandExecutor<Void> {

    @Autowired
    WorkflowBuildTimeService workflowBuildTimeService;

    @Autowired
    WorkflowRuntimeService workflowRuntimeService;

    @Autowired
    ProcessService processService;

    @SneakyThrows
    @Override
    @Transactional
    public Void run() {
        ClaimUserTaskCommand command = this.getCommand().as(ClaimUserTaskCommand.class);

        WorkflowTaskInstance workflowTaskInstance = workflowRuntimeService.query().getWorkflowTaskInstanceById(command.getWorkflowTaskInstanceId());
        List<String> sourceAssigneeUserIds = workflowTaskInstance.getAssigneeUserIds();
        List<String> sourceAssigneeGroupIds = workflowTaskInstance.getAssigneeGroupIds();

        processService.claimTask(workflowTaskInstance.getProcessTaskId(), command.getExecutorId());
        workflowTaskInstance.setAssigneeUserIds(Lists.newArrayList(command.getExecutorId()));
        workflowTaskInstance.setAssigneeGroupIds(Lists.newArrayList());

        // 发送事件
        this.getWorkflowInstanceAware().handleEvent(this.getProcessDefKey(), UserTaskClaimEvent.builder()
                .executorId(command.getExecutorId())
                .sourceAssigneeUserIds(sourceAssigneeUserIds)
                .sourceAssigneeGroupIds(sourceAssigneeGroupIds)
                .workflowInstanceId(command.getWorkflowInstanceId())
                .workflowTaskInstanceId(command.getWorkflowTaskInstanceId())
                .build());
        return null;
    }
}
