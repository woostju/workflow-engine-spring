package com.github.bryx.workflow.command;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.command.executor.StartWorkflowInstanceCommandExecutor;
import com.github.bryx.workflow.domain.WorkflowInstanceRelation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = StartWorkflowInstanceCommandExecutor.class)
public class StartWorkflowInstanceCommand<T> extends BaseCommand {
    /**
     * 表单数据
     */
    private JSONObject formData;
    /**
     * 执行人id
     */
    private String executorId;
    /**
     * 受理人id
     */
    private List<String> assignUserIds;

    /**
     * 受理组id
     */
    private List<String> assigneeGroupIds;

    /**
     * 父流程任务id
     */
    private String parentWorkflowTaskInstanceId;

    /**
     * 父子流程关系
     */
    private WorkflowInstanceRelation.Type workflowInstanceRelation;


    @Override
    public CommandType getType() {
        return CommandType.START_COMMAND;
    }

}
