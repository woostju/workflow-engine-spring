package com.github.bryx.workflow.command.executor;

import com.github.bryx.workflow.command.TimerTriggerCommand;
import com.github.bryx.workflow.domain.WorkflowDef;
import com.github.bryx.workflow.event.TimerTriggerEvent;
import com.github.bryx.workflow.handler.WorkflowInstanceAware;
import com.github.bryx.workflow.service.WorkflowBuildTimeService;
import com.github.bryx.workflow.service.WorkflowRuntimeService;
import com.github.bryx.workflow.service.process.ProcessService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TimerTriggerCommandExecutor extends CommandExecutor<Void> {

    @Autowired
    WorkflowBuildTimeService workflowBuildTimeService;

    @Autowired
    WorkflowRuntimeService workflowRuntimeService;

    @Autowired
    ProcessService processService;

    @SneakyThrows
    @Override
    @Transactional
    public Void run() {
        TimerTriggerCommand command = this.getCommand().as(TimerTriggerCommand.class);

        this.getWorkflowInstanceAware().handleEvent(this.getProcessDefKey(), TimerTriggerEvent.builder()
                .workflowInstance(command.getWorkflowInstance())
                .workflowTaskInstance(command.getWorkflowTaskInstance())
                .timerInstance(command.getWorkflowTimerInstance())
                .timerJob(command.getWorkflowTimerJob())
                .build());
        return null;
    }
}
