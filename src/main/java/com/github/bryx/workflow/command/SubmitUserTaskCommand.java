package com.github.bryx.workflow.command;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.command.executor.SubmitUserTaskCommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = SubmitUserTaskCommandExecutor.class)
public class SubmitUserTaskCommand extends BaseCommand {
    /**
     * 执行人id
     */
    private String executorId;
    /**
     * 表单数据
     */
    private JSONObject formData;
    /**
     * 受理人ids
     */
    private List<String> assigneeUserIds;
    /**
     * 受理组ids
     */
    private List<String> assigneeGroupIds;

    @Override
    public CommandType getType() {
        return CommandType.SUBMIT_COMMAND;
    }

}
