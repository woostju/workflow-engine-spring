package com.github.bryx.workflow.command;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.command.executor.ModifyUserTaskCommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = ModifyUserTaskCommandExecutor.class)
public class ModifyUserTaskCommand extends BaseCommand {
    /**
     * 执行人id
     */
    private String executorId;
    /**
     * 表单数据
     */
    private JSONObject formData;

    @Override
    public CommandType getType() {
        return CommandType.MODIFY_COMMAND;
    }
}
