package com.github.bryx.workflow.command;

import com.github.bryx.workflow.command.executor.ClaimUserTaskCommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 *
 * 认领任务
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@CommandConfiguration(executor = ClaimUserTaskCommandExecutor.class)
public class ClaimUserTaskCommand extends BaseCommand {
    /**
     * 认领人
     */
    private String executorId;

    @Override
    public CommandType getType() {
        return CommandType.CLAIM_COMMAND;
    }
}
