package com.github.bryx.workflow.referenceobject;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @Author jameswu
 * @Date 2021/7/20
 **/
public interface ReferenceObjectSupport {
    /**
     * 返回引用对象的ids
     */
    public Map<Class, Collection> referenceObjectIds();
}
