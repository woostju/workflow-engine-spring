package com.github.bryx.workflow.referenceobject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * 加载器
 * @Author jameswu
 * @Date 2021/6/11
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Loader {
    Class[] type();
}
