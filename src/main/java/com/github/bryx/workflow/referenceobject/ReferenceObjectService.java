package com.github.bryx.workflow.referenceobject;

import java.util.Collection;
import java.util.Map;

/**
 * @Author jameswu
 * @Date 2021/7/21
 **/
public interface ReferenceObjectService {

    /**
     *
     * 加载引用对象数据加载器
     * @param clazz
     * @param loader
     */
    public void registerLoader(Class clazz, ReferenceObjectLoader loader);

    /**
     *
     * 获取引用对象数据加载器
     * @param clazz
     */
    public ReferenceObjectLoader getLoader(Class clazz);

    /**
     *
     * 获取对象的引用对象组合
     * @param support
     */
    public Map<String, Map<String, Object>> loadReferenceObjects(ReferenceObjectSupport support);

    /**
     *
     * 获取对象数组的引用对象组合
     * @param supports
     */
    public Map<String, Map<String, Object>> loadReferenceObjects(Collection<? extends ReferenceObjectSupport> supports);

}
