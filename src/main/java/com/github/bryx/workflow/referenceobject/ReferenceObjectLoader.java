package com.github.bryx.workflow.referenceobject;

import java.util.Collection;
import java.util.Map;

/**
 * @Author jameswu
 * @Date 2021/7/20
 **/
public interface ReferenceObjectLoader<T, R> {
    /**
     * 根据ids加载type类型的数据
     * @param type
     * @param ids
     */
    public Map<String, R> loadObjects(Class type, Collection<T> ids);
}
