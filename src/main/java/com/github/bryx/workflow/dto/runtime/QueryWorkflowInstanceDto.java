package com.github.bryx.workflow.dto.runtime;

import com.github.bryx.workflow.domain.WorkflowInstance;
import com.github.bryx.workflow.dto.PageSupport;
import lombok.Data;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
public class QueryWorkflowInstanceDto<T> extends PageSupport<T> {

    private String keyword;

    private List<String> ids;

    private List<WorkflowInstance.WorkflowInstanceStatus> statuses;
}
