package com.github.bryx.workflow.dto.runtime;

import com.github.bryx.workflow.domain.WorkflowTimerJob;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UpdateWorkflowTimerJobDto {
    @ApiModelProperty(value = "job id")
    List<String> ids;

    @ApiModelProperty(value = "任务实例id")
    List<String> workflowTaskIds;

    @ApiModelProperty(value = "流程实例id")
    List<String> workflowInstanceIds;

    @ApiModelProperty(value = "状态", required = true)
    WorkflowTimerJob.Status status;
}
