package com.github.bryx.workflow.dto.runtime;

/**
 * @Author jameswu
 * @Date 2021/6/29
 **/
public enum WorkflowTimerType {
    DURATION,
    CYCLE,
    CRON,
    FIXED_DATE
}
