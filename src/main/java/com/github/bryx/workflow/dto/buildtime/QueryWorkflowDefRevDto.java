package com.github.bryx.workflow.dto.buildtime;

import com.github.bryx.workflow.domain.WorkflowDefRev;
import com.github.bryx.workflow.dto.PageSupport;
import lombok.Data;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/2
 **/
@Data
public class QueryWorkflowDefRevDto<T> extends PageSupport<T> {

    List<WorkflowDefRev.WorkflowDefRevStatus> statuses;

    String defId;

    String defRevId;

    Boolean deleted = false;
}
