package com.github.bryx.workflow.dto.runtime;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.domain.WorkflowInstanceRelation;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateWorkflowInstanceDto{

    public static class ValidateCreateWorkflow{

    }

    public static class ValidateCreateSubworkflow{

    }

    @ApiModelProperty(value = "模型定义id")
    @NotBlank(message = "模型定义id不能为空")
    String defId;

    @ApiModelProperty(value = "模型定义版本id")
    @NotBlank(message = "模型定义版本id不能为空")
    String defRevId;

    @ApiModelProperty(value = "activiti process id")
    @NotBlank(message = "activiti process id不能为空")
    String processId;

    @ApiModelProperty(value = "创建人id")
    @NotBlank(message = "创建人id不能为空")
    String creatorId;

    @ApiModelProperty(value = "表单数据")
    JSONObject formData;

    @ApiModelProperty(value = "父流程实例节点")
    @NotBlank(message = "父流程任务实例不能为空", groups = {ValidateCreateSubworkflow.class})
    String parentWorkflowTaskInstanceId;

    @ApiModelProperty(value = "父子流程关系类型")
    @NotBlank(message = "父子流程关系类型不能为空", groups = {ValidateCreateSubworkflow.class})
    WorkflowInstanceRelation.Type workflowInstanceRelationType;
}
