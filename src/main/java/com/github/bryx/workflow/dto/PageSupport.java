package com.github.bryx.workflow.dto;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

/**
 * @Author jameswu
 * @Date 2021/6/3
 **/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class PageSupport<T>{

    private Long pageNum;

    private Long pageSize;

    private SFunction<T,?> orderField;

    private Boolean asc ;

    public Long getPageNum() {
        return Optional.ofNullable(this.pageNum).orElse(0l);
    }

    public Boolean getAsc() {
        return Optional.ofNullable(this.asc).orElse(false);
    }

    public Long getPageSize() {
        return Optional.ofNullable(this.pageSize).orElse(Long.MAX_VALUE);
    }

    public Page<T> page(){
        Page<T> page = new Page<>();
        page.setCurrent(this.getPageNum());
        page.setSize(this.getPageSize());
        return page;
    }


}
