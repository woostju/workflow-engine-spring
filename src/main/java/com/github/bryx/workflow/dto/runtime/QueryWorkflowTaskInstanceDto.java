package com.github.bryx.workflow.dto.runtime;

import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import com.github.bryx.workflow.dto.PageSupport;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class QueryWorkflowTaskInstanceDto<T> extends PageSupport<T> {

    private List<WorkflowTaskInstance.WorkflowTaskInstanceStatus> statuses;

    private List<String> taskDefIds;

    private List<String> workflowInstanceIds;

    private List<String> workflowTaskInstanceIds;

}
