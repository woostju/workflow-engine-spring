package com.github.bryx.workflow.dto.runtime;

import com.github.bryx.workflow.domain.WorkflowInstance;
import com.github.bryx.workflow.dto.PageSupport;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class QuerySubworkflowInstanceDto<T> extends PageSupport<T> {

    /**
     * 查询父流程id下发起的子流程
     */
    private List<String> parentWorkflowInstanceIds;

    /**
     * 通过状态查询子流程
     */
    private List<WorkflowInstance.WorkflowInstanceStatus> statuses;

    /**
     * 查询父流程任务实例id上发起的子流程
     */
    private List<String> parentWorkflowTaskInstanceIds;

    /**
     * 查询父流程任务节点上发起的子流程
     */
    private List<String> parentProcessTaskDefIds;
}
