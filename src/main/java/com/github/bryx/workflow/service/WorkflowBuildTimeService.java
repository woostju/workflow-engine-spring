package com.github.bryx.workflow.service;

/**
 *
 * 先创建一个流程定义草稿，然后做编辑，编辑完成后，发布流程定义
 * 1：每次发布流程定义都会创建新的流程定义版本
 * 2：新发起的流程实例将使用最新的流程定义版本
 * 3：已经发起的流程实例继续在其对应的流程定义版本上运行
 *
 * 不同版本共享一个流程定义的id，seq，name，createTime等基础属性
 * 不同版本有各自的流程模型及表单
 *
 *  @Author jameswu
 *  @Date 2021/6/2
 **/
public interface WorkflowBuildTimeService {

    public WorkflowBuildTimeManager manager();

    public WorkflowBuildTimeQuery query();
}
