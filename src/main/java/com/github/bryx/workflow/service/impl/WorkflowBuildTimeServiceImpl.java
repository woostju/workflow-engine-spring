package com.github.bryx.workflow.service.impl;

import com.github.bryx.workflow.service.WorkflowBuildTimeManager;
import com.github.bryx.workflow.service.WorkflowBuildTimeQuery;
import com.github.bryx.workflow.service.WorkflowBuildTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author jameswu
 * @Date 2021/6/2
 **/
@Service
public class WorkflowBuildTimeServiceImpl implements WorkflowBuildTimeService {
    @Autowired
    WorkflowBuildTimeQuery workflowBuildTimeQuery;

    @Autowired
    WorkflowBuildTimeManager workflowBuildTimeManager;

    @Override
    public WorkflowBuildTimeQuery query() {
        return this.workflowBuildTimeQuery;
    }

    @Override
    public WorkflowBuildTimeManager manager() {
        return workflowBuildTimeManager;
    }
}
