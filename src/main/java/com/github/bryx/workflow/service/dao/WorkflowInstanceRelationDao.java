package com.github.bryx.workflow.service.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.bryx.workflow.domain.WorkflowInstanceRelation;
import com.github.bryx.workflow.mapper.WorkflowInstanceRelationMapper;
import org.springframework.stereotype.Service;

/**
 * @Author jameswu
 * @Date 2021/6/2
 **/
@Service
public class WorkflowInstanceRelationDao extends ServiceImpl<WorkflowInstanceRelationMapper, WorkflowInstanceRelation> {

}
