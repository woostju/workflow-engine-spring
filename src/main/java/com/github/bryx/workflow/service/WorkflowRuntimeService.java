package com.github.bryx.workflow.service;

import com.alibaba.fastjson.JSONObject;
import com.github.bryx.workflow.domain.*;

import java.util.List;
import java.util.Map;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
public interface WorkflowRuntimeService {


    /**
     * 根据processDefType启动流程实例
     * @param processKey
     * @param formData
     * @param executorId
     * @return workflowInstanceId
     */
    public String start(String processKey, JSONObject formData, String executorId);

    /**
     *
     * 启动流程实例, 并指定受理人
     * @param processKey
     * @param formData
     * @param assignUserIds
     * @param assigneeGroupIds
     * @param executorId
     * @return workflowInstanceId
     */
    public String startAndAssign(String processKey, JSONObject formData, List<String> assignUserIds, List<String> assigneeGroupIds, String executorId);

    /**
     * 主流程上启动子流程
     * @param parentWorkflowTaskInstanceId
     * @param processKey
     * @param formData
     * @param executorId
     * @param workflowRelation
     * @return workflowInstanceId
     */
    public String startSubworkflow(String parentWorkflowTaskInstanceId, String processKey, JSONObject formData, String executorId, WorkflowInstanceRelation.Type workflowRelation) ;

    /**
     * 主流程上启动子流程, 并指定受理人
     * @param parentWorkflowTaskInstanceId
     * @param processKey
     * @param formData
     * @param executorId
     * @param workflowRelation
     * @return workflowInstanceId
     */
    public String startSubworkflowAndAssign(String parentWorkflowTaskInstanceId, String processKey, JSONObject formData, List<String> assignUserIds, List<String> assigneeGroupIds, String executorId, WorkflowInstanceRelation.Type workflowRelation) ;

    /**
     * 驳回流程
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param executorId
     * @return 新的workflowTaskInstanceIds
     */
    public List<String> rejectBack(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, String executorId);

    /**
     * 驳回并指定受理人
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param assignUserIds
     * @param assigneeGroupIds
     * @param executorId
     * @return 新的workflowTaskInstanceIds
     */
    public List<String> rejectBackAndAssign(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, List<String> assignUserIds, List<String> assigneeGroupIds, String executorId);

    /**
     * 抢单，受理人是executorId
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param executorId
     */
    public void claim(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, String executorId);

    /**
     * 转交受理人
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param assignUserIds
     * @param assigneeGroupIds
     * @param executorId
     */
    public void transfer(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, List<String> assignUserIds, List<String> assigneeGroupIds, String executorId);

    /**
     * 编辑保存流程实例
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param executorId
     */
    public void modify(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, String executorId);

    /**
     * 提交用户任务
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param executorId
     * @return 新的workflowTaskInstanceIds
     */
    public List<String> submit(String workflowInstanceId, String workflowInstanceTaskId, JSONObject formData, String executorId);

    /**
     * 提交用户任务并指定受理人
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param formData
     * @param assignUserIds
     * @param assigneeGroupIds
     * @param executorId
     * @return 新的workflowTaskInstanceIds
     */
    public List<String> submitAndAssign(String workflowInstanceId, String workflowInstanceTaskId,JSONObject formData, List<String> assignUserIds, List<String> assigneeGroupIds, String executorId);

    /**
     * 关闭流程实例
     * @param workflowInstanceId
     * @param workflowInstanceTaskId
     * @param executorId
     */
    public void close(String workflowInstanceId, String workflowInstanceTaskId, String executorId);

    /**
     * 触发器被触发，请不要直接调用
     * @param workflowInstance
     * @param workflowTaskInstance
     * @param workflowTimerInstance
     * @param workflowTimerJob
     */
    void timerTriggered(WorkflowInstance workflowInstance, WorkflowTaskInstance workflowTaskInstance, WorkflowTimerInstance workflowTimerInstance, WorkflowTimerJob workflowTimerJob);

    /**
     * @return 查询器
     */
    public WorkflowRuntimeQuery query();
    /**
     * @return 管理器
     */
    public WorkflowRuntimeManager manager();

    public Map<String, WorkflowDefProcessConfig.UserTaskConfig> userTaskConfig(String processKey, String workflowInstanceId, String userId);
}
