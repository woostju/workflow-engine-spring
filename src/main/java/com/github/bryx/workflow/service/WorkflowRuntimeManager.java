package com.github.bryx.workflow.service;

import com.github.bryx.workflow.domain.WorkflowTaskInstance;
import com.github.bryx.workflow.dto.runtime.*;
import org.springframework.validation.annotation.Validated;

/**
 * @Author jameswu
 * @Date 2021/6/10
 **/
public interface WorkflowRuntimeManager {

    /**
     * 创建WorkflowInstance
     * @param dto
     * @return
     */
    public String createWorkflowInstance(@Validated(CreateWorkflowInstanceDto.ValidateCreateWorkflow.class) CreateWorkflowInstanceDto dto);

    public void delete(String workflowInstanceId, String executorId);
    /**
     * 保存流程实例, 如修改实例的表单数据,及状态
     * @param dto
     */
    public void updateWorkflowInstance(UpdateWorkflowInstanceDto dto);

    public WorkflowTaskInstance createWorkflowTaskInstance(CreateWorkflowTaskInstanceDto dto);

    public String createWorkflowTimerJob(CreateWorkflowTimerJobDto createWorkflowTimerJobDto);

    public void updateWorkflowTimerJob(UpdateWorkflowTimerJobDto updateWorkflowTimerJobDto);

    public void updateWorkflowTaskInstance(UpdateWorkflowTaskInstanceDto dto);

    public String createWorkflowInstActivity(CreateWorkflowInstanceActivityDto createWorkflowTimerJobDto);
}
