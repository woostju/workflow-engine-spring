package com.github.bryx.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.bryx.workflow.domain.WorkflowInstance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Author jameswu
 * @Date 2021/6/2
 **/
@Mapper
public interface WorkflowInstanceMapper extends BaseMapper<WorkflowInstance> {
    public String getProcessDefKeyOfWorkflowInstance(@Param("id") String id);
}
